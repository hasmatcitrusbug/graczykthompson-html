/* Set the width of the side navigation to 250px */

function openNav() {
  document.getElementById("mySidenav").style.width = "100%";
  document.getElementById("nav-res").style.opacity = "1";
  document.getElementById("cd-shadow-layer").style.display = "block";
  
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("nav-res").style.opacity = "0";
  document.getElementById("cd-shadow-layer").style.display = "none";  
  //$('#nav-res').hide().fadeIn('slow');
} 

$(document).ready(function(){ 
  $("#cd-shadow-layer").click(function(){
    //$("#cd-shadow-layer").addClass("display-none");
    closeNav(); 
  });
});

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});



$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(200);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(200);
});


var x, i, j, selElmnt, a, b, c;
/* Look for any elements with the class "customs-select": */
x = document.getElementsByClassName("customs-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}





function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);


$( function() {
  var availableTags = [
    "ActionScript",
    "AppleScript",
    "Asp",
    "BASIC",
    "C",
    "C++",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Erlang",
    "Fortran",
    "Groovy",
    "Haskell",
    "Java",
    "JavaScript",
    "Lisp",
    "Perl",
    "PHP",
    "Python",
    "Ruby",
    "Scala",
    "Scheme"
  ];
  function split( val ) {
    return val.split( /,\s*/ );
  }
  function extractLast( term ) {
    return split( term ).pop();
  }

  $( "#tags" )
    // don't navigate away from the field on tab when selecting an item
    .on( "keydown", function( event ) {
      if ( event.keyCode === $.ui.keyCode.TAB &&
          $( this ).autocomplete( "instance" ).menu.active ) {
        event.preventDefault();
      }
    })
    .autocomplete({
      minLength: 0,
      source: function( request, response ) {
        // delegate back to autocomplete, but extract the last term
        response( $.ui.autocomplete.filter(
          availableTags, extractLast( request.term ) ) );
      },
      focus: function() {
        // prevent value inserted on focus
        return false;
      },
      select: function( event, ui ) {
        var terms = split( this.value );
        // remove the current input
        terms.pop();
        // add the selected item
        terms.push( ui.item.value );
        // add placeholder to get the comma-and-space at the end
        terms.push( "" );
        this.value = terms.join( ", " );
        return false;
      }
    });
} );

/*
	Dropdown with Multiple checkbox select with jQuery
*/

$(".dropdown-pt dt a").on('click', function() {
  $(".dropdown-pt dd ul").slideToggle('fast');
});

$(".dropdown-pt dd ul li a").on('click', function() {
  $(".dropdown-pt dd ul").hide();
});

function getSelectedValue(id) {
  return $("#" + id).find("dt a span.value").html();
}

$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown-pt")) $(".dropdown-pt dd ul").hide();
});

$('.ms-pt input[type="checkbox"]').on('click', function() {

  var title = $(this).closest('.ms-pt').find('input[type="checkbox"]').val(),
    title = $(this).val() + ",";

  if ($(this).is(':checked')) {
    var html = '<span title="' + title + '">' + title + '</span>';
    $('.multiSel-pt').append(html);
    $(".hida-pt").show();
  } else {
    $('span[title="' + title + '"]').remove();
    var ret = $(".hida-pt");
    $('.dropdown-pt dt a').append(ret);

  }
});


$(".dropdown-baths dt a").on('click', function() {
  $(".dropdown-baths dd ul").slideToggle('fast');
});

$(".dropdown-baths dd ul li a").on('click', function() {
  $(".dropdown-baths dd ul").hide();
});

function getSelectedValue(id) {
  return $("#" + id).find("dt a span.value").html();
}

$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown-baths")) $(".dropdown-baths dd ul").hide();
});

$('.ms-baths input[type="checkbox"]').on('click', function() {

  var title = $(this).closest('.ms-baths').find('input[type="checkbox"]').val(),
    title = $(this).val() + ",";

  if ($(this).is(':checked')) {
    var html = '<span title="' + title + '">' + title + '</span>';
    $('.multiSel-baths').append(html);
    $(".hida-baths").show();
  } else {
    $('span[title="' + title + '"]').remove();
    var ret = $(".hida-baths");
    $('.dropdown-baths dt a').append(ret);

  }
});


$(".dropdown-beds dt a").on('click', function() {
  $(".dropdown-beds dd ul").slideToggle('fast');
});

$(".dropdown-beds dd ul li a").on('click', function() {
  $(".dropdown-beds dd ul").hide();
});

function getSelectedValue(id) {
  return $("#" + id).find("dt a span.value").html();
}

$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown-beds")) $(".dropdown-beds dd ul").hide();
});

$('.ms-beds input[type="checkbox"]').on('click', function() {

  var title = $(this).closest('.ms-beds').find('input[type="checkbox"]').val(),
    title = $(this).val() + ",";

  if ($(this).is(':checked')) {
    var html = '<span title="' + title + '">' + title + '</span>';
    $('.multiSel-beds').append(html);
    $(".hida-beds").show();
  } else {
    $('span[title="' + title + '"]').remove();
    var ret = $(".hida-beds");
    $('.dropdown-beds dt a').append(ret);

  }
});

$(".dropdown-pmin dt a").on('click', function() {
  $(".dropdown-pmin dd ul").slideToggle('fast');
});

$(".dropdown-pmin dd ul li a").on('click', function() {
  $(".dropdown-pmin dd ul").hide();
});

function getSelectedValue(id) {
  return $("#" + id).find("dt a span.value").html();
}

$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown-pmin")) $(".dropdown-pmin dd ul").hide();
});

$('.ms-pmin li').on('click', function() {

  var title = $(this).find('input[type="radio"]').val();

  var html = '<span title="' + title + '">' + title + '</span>';
    $('.multiSel-pmin').html(html);
    $(".hida-pmin").show();
  //console.log(title)  

   
});


$(".dropdown-pmax dt a").on('click', function() {
  $(".dropdown-pmax dd ul").slideToggle('fast');
});

$(".dropdown-pmax dd ul li a").on('click', function() {
  $(".dropdown-pmax dd ul").hide();
});

function getSelectedValue(id) {
  return $("#" + id).find("dt a span.value").html();
}

$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown-pmax")) $(".dropdown-pmax dd ul").hide();
});


$('.ms-pmax li').on('click', function() {

  var title = $(this).find('input[type="radio"]').val();

  var html = '<span title="' + title + '">' + title + '</span>';
    $('.multiSel-pmax').html(html);
    $(".hida-pmax").show();
  //console.log(title)  

   
});




var x, i, j, selElmnt, a, b, c;
/* Look for any elements with the class "customs-select": */
x = document.getElementsByClassName("customs-select-sort");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}